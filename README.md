# README #

This is an initial apache stack docker setup containing php, mysql, apache multiple hosts and phpmyadmin

### What is this repository for? ###

* This is an out of the box docker setup
* Version 1.0

### How do I get set up? ###

* Install docker and docker-compose
* Go to the root folder of this project
* Configure your host settings in 000-default.conf
* Execute start.sh to start the server
* Execute stop.sh to stop the server

### Who do I talk to? ###

* All docker apache interested people